//
//  ViewController.m
//  DisplayInfoTVC
//
//  Created by Sword Software on 17/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"
#import "TargetViewControllerb.h"
@interface ViewController () <UITextFieldDelegate>
@property (nonatomic, strong) NSString *textFieldValue;
@end

@implementation ViewController

//@synthesize textBoxName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.textBoxName.delegate = self;
}


- (IBAction)submit:(id)sender {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.textFieldValue = textField.text;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self.view endEditing:YES];
    TargetViewControllerb *targetViewController = segue.destinationViewController;
    
    NSLog(@"Text field value = %@", self.textBoxName.text);
    
    NSString *try = self.textBoxName.text;
    NSLog(@"Text field value = %@",try);
    
    targetViewController.strName = self.textFieldValue;
    NSLog(@"Text field value = %@",targetViewController.strName);
}


@end
