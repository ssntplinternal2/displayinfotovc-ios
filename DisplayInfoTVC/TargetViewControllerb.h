//
//  TargetViewControllerb.h
//  DisplayInfoTVC
//
//  Created by Sword Software on 25/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TargetViewControllerb : UIViewController

@property (weak, nonatomic) NSString *strName;

@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end

NS_ASSUME_NONNULL_END
