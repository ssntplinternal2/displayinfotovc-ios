//
//  ViewController.h
//  DisplayInfoTVC
//
//  Created by Sword Software on 17/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textBoxName;


- (IBAction)submit:(id)sender;
@end

